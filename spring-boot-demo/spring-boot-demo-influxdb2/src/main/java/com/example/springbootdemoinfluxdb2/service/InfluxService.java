package com.example.springbootdemoinfluxdb2.service;

import com.example.springbootdemoinfluxdb2.model.Cpu;
import com.example.springbootdemoinfluxdb2.model.Temperature;
import com.example.springbootdemoinfluxdb2.repo.InfluxRepository;
import com.influxdb.query.FluxTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * @ProjectName: MyJava
 * @ClassName: InfluxService
 * @Description: service.InfluxService
 * @Author: zhangdi
 * @Date: 2021年10月14日 14:14
 **/
@Service
public class InfluxService {
    InfluxRepository influxRepository;

    @Autowired
    public void setInfluxRepository(InfluxRepository repository) {
        this.influxRepository = repository;
    }

    public void writeData1() {
        influxRepository.writeData(null, new HashMap<String, Object>() {{
            put("temperature", 600);
            //put("humidity", 800.03);
        }});
    }

    public void writeCpu() throws InterruptedException {
        for (int i = 0; i < 15; i++) {
            int finalI = i;
            influxRepository.writeDataTags("cpu", new HashMap<String, Object>(2) {{
                put("fieldKey", finalI * 10);
            }}, new HashMap<String, String>(2) {{
                put("Ident", "10.201.52.33");
            }});
            influxRepository.writeDataTags("memory", new HashMap<String, Object>(2) {{
                put("fieldKey", finalI * 15);
            }}, new HashMap<String, String>(2) {{
                put("Ident", "10.201.52.33");
            }});
            influxRepository.writeDataTags("uptime", new HashMap<String, Object>(2) {{
                put("fieldKey", finalI * 30);
            }}, new HashMap<String, String>(2) {{
                put("Ident", "10.201.52.33");
            }});

            System.out.println("第" + i + 1 + "次");
            Thread.sleep(5000);
        }
    }

    public boolean writeDataBatch() {
        return influxRepository.writeDataBatch();
    }

    public boolean writeDataPoJo() {
        return influxRepository.writeOneDataPoJo();
    }

    public List<Temperature> queryDataByPojo() {
        YamlPropertiesFactoryBean yamlPropertiesFactoryBean = new YamlPropertiesFactoryBean();
        yamlPropertiesFactoryBean.setResources(new ClassPathResource("application.yml"));
        String property = yamlPropertiesFactoryBean.getObject().getProperty("spring.influx.bucket");
        System.out.println(property);
        return influxRepository.queryDataByPojo();
    }

    public List<Cpu> queryDataByCpuPojo() {
        return influxRepository.queryDataByCpuPojo();
    }

    public List<FluxTable> queryFluxTable() {
        return influxRepository.queryFluxTable();
    }
}
