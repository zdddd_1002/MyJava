package com.example.springbootdemoinfluxdb2.config;

import com.influxdb.LogLevel;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ProjectName: MyJava
 * @ClassName: InfluxConfig
 * @Description: InfluxConfig
 * @Author: zhangdi
 * @Date: 2021年10月14日 09:22
 **/
@Configuration
public class InfluxConfig {

    /**
     * url
     */
    @Value("${spring.influx.url:''}")
    private String url;

    /**
     * token
     */
    @Value("${spring.influx.token:''}")
    private String token;

    /**
     * org
     */
    @Value("${spring.influx.org:''}")
    private String org;

    /**
     * bucket
     */
    @Value("${spring.influx.bucket:''}")
    private String bucket;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    @Bean
    public InfluxDBClient influxDb() {
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(url, token.toCharArray(), org, bucket);
        influxDBClient.setLogLevel(LogLevel.BASIC);
        return influxDBClient;
    }
}
