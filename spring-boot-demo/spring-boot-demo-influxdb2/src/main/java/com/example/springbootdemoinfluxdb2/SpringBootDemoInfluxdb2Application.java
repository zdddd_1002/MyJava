package com.example.springbootdemoinfluxdb2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDemoInfluxdb2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoInfluxdb2Application.class, args);
    }

}
