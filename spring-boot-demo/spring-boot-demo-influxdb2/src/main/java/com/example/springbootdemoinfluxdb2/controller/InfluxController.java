package com.example.springbootdemoinfluxdb2.controller;

import com.example.springbootdemoinfluxdb2.model.Cpu;
import com.example.springbootdemoinfluxdb2.model.Temperature;
import com.example.springbootdemoinfluxdb2.service.InfluxService;
import com.influxdb.query.FluxTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ProjectName: MyJava
 * @ClassName: InfluxController
 * @Description: controller.InfluxController
 * @Author: zhangdi
 * @Date: 2021年10月14日 11:15
 **/
@RestController
public class InfluxController {
    InfluxService influxService;

    @Autowired
    public void setInfluxService(InfluxService influxService) {
        this.influxService = influxService;
    }

    @RequestMapping(value = "writeData", method = RequestMethod.GET)
    public void writeData() {
        influxService.writeData1();
    }

    @RequestMapping(value = "writeCpu", method = RequestMethod.GET)
    public void writeCpu() throws InterruptedException {
        influxService.writeCpu();
    }

    @RequestMapping(value = "writeDataPoJo", method = RequestMethod.GET)
    public boolean writeDataPoJo() {
        return influxService.writeDataPoJo();
    }

    @RequestMapping(value = "writeDataBatch", method = RequestMethod.GET)
    public boolean writeDataBatch() {
        return influxService.writeDataBatch();
    }

    @RequestMapping(value = "queryDataByPojo", method = RequestMethod.GET)
    public List<Temperature> queryDataByPojo() {
        return influxService.queryDataByPojo();
    }

    @RequestMapping(value = "queryDataByCpuPojo", method = RequestMethod.GET)
    public List<Cpu> queryDataByCpuPojo() {
        return influxService.queryDataByCpuPojo();
    }

    @RequestMapping(value = "queryFluxTable", method = RequestMethod.GET)
    public List<FluxTable> queryFluxTable() {
        return influxService.queryFluxTable();
    }
}
