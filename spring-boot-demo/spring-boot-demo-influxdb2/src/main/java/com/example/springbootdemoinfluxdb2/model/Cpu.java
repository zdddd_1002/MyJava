package com.example.springbootdemoinfluxdb2.model;

import com.influxdb.annotations.Column;
import com.influxdb.annotations.Measurement;
import lombok.Data;

import java.time.Instant;

/**
 * @ProjectName: MyJava
 * @ClassName: Cpu
 * @Description: Cpu
 * @Author: zhangdi
 * @Date: 2021年11月11日 10:12
 **/
@Data
@Measurement(name = "cpu")
public class Cpu {
    @Column(tag = true)
    private String Ident;

    @Column(name = "value")
    Long value;

    @Column(timestamp = true)
    Instant time;
}
