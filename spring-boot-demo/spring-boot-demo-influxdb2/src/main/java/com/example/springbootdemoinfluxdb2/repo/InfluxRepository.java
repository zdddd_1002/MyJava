package com.example.springbootdemoinfluxdb2.repo;

import com.example.springbootdemoinfluxdb2.model.Cpu;
import com.example.springbootdemoinfluxdb2.model.Temperature;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.WriteApiBlocking;
import com.influxdb.client.WriteOptions;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.time.Instant;
import java.util.*;

/**
 * @ProjectName: MyJava
 * @ClassName: InfluxRepository
 * @Description: repo.InfluxRepository
 * @Author: zhangdi
 * @Date: 2021年10月14日 14:28
 **/
@Repository
public class InfluxRepository {
    InfluxDBClient influxDBClient;

    @Autowired
    public void setInfluxDBClient(InfluxDBClient influxDBClient) {
        this.influxDBClient = influxDBClient;
    }

    public void writeData(String measurement, Map<String, Object> fields) {
        WriteApiBlocking writeApiBlocking = influxDBClient.getWriteApiBlocking();

        Assert.notNull(measurement, "measurement is not null!");
        System.out.println("not execute!");
        Point point = Point.measurement(measurement).addFields(fields).time(Instant.now(), WritePrecision.S);
        writeApiBlocking.writePoint(point);
    }

    public void writeDataTags(String measurement, Map<String, Object> fields, Map<String, String> tags) {
        WriteApiBlocking writeApiBlocking = influxDBClient.getWriteApiBlocking();

        Assert.notNull(measurement, "measurement is not null!");
        System.out.println("not execute!");
        Point point = Point.measurement(measurement).addFields(fields).addTags(tags).time(Instant.now(),
                WritePrecision.S);
        writeApiBlocking.writePoint(point);
    }

    public boolean writeDataBatch() {
        WriteOptions writeOptions = WriteOptions.builder()
                .batchSize(5000)
                .flushInterval(1000)
                .bufferLimit(10000)
                .jitterInterval(1000)
                .retryInterval(5000)
                .build();
        // 写入优化
        //1、使用 gzip 压缩
        //2、批量写，最理想的是一次 5000 行
        //3、转换为行的时候，按字典顺序排序 tag 的字段
        //4、设置最合适的时间精度。例如同一个 tag 不会有两条同一秒的数据，那么就把时间精度设置为秒级

        // *****具有相同测量名称、标签集和时间戳的点，InfluxDB 创建新旧字段集的联合(重复数据包）
        // 单条写入
        // WriteApi writeApi = influxDBClient.makeWriteApi(writeOptions);
        WriteApiBlocking writeApiBlocking = influxDBClient.getWriteApiBlocking();

        // 批量写入
        List<Point> points = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            // tag 仅支持字符串
            // field 存储数值
            int finalI = i;
            Point point = Point.measurement("sensor").addFields(new HashMap<String, Object>(1) {{
                put("field" + finalI, new Random().nextInt(100));
            }}).addTags(new HashMap<String, String>(1) {{
                put("myTag", "tagValue");
            }}).time(Instant.now(), WritePrecision.S);
            points.add(point);
        }
        writeApiBlocking.writePoints(points);
        //writeApi.close();
        return true;
    }

    public boolean writeOneDataPoJo() {
        // 创建一个新的同步阻塞写客户端
        WriteApiBlocking writeApiBlocking = influxDBClient.getWriteApiBlocking();
        Temperature temperature = new Temperature();
        temperature.setLocation("south");
        temperature.setValue(300L);
        temperature.setTime(Instant.now());
        writeApiBlocking.writeMeasurement(WritePrecision.S, temperature);
        return true;
    }

    /**
     * 标签已经加入索引，字段未加入索引->标签tag的查询效率要比字段field更高。
     * 1、将经常查询的元数据存储在标签中。
     * 2、如果每个数据点包含不同的值，则将数据存储在字段中。
     * 3、将数值存储为字段（标签值仅支持字符串值）。
     *
     * @return
     */
    public List<Temperature> queryDataByPojo() {
        // 不太实用
        String fluxQl = "from(bucket:\"mydb\") |> range(start:-20h)";
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<Temperature> temperatures = queryApi.query(fluxQl, Temperature.class);
        return temperatures;
    }

    public List<Cpu> queryDataByCpuPojo() {
        // 不太实用
        String fluxQl = "from(bucket: \"mydb\")|> range(start:-24h)|> filter(fn: (r) => " +
                "r[\"_measurement\"] == \"memory\")";
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<Cpu> Cpu = queryApi.query(fluxQl, Cpu.class);
        return Cpu;
    }

    /**
     * 查询：三要素
     * 1、一个数据源
     * 2、一个时间范围（无界查询非常耗费资源，时间范围作为保护措施进行查询）
     * 3、数据过滤器
     *
     * @return
     */
    public List<FluxTable> queryFluxTable() {
        String fluxQl = "from(bucket:\"mydb\") |> range(start:-2h)";
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> fluxTables = queryApi.query(fluxQl);

        return fluxTables;
    }
}
