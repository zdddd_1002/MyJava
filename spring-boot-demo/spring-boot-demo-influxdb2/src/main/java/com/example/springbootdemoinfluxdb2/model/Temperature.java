package com.example.springbootdemoinfluxdb2.model;

import com.influxdb.annotations.Column;
import com.influxdb.annotations.Measurement;
import lombok.Data;

import java.time.Instant;

/**
 * @ProjectName: MyJava
 * @ClassName: Temperature
 * @Description:Temperature
 * @Author: zhangdi
 * @Date: 2021年09月28日 14:40
 **/
@Data
@Measurement(name = "temperature")
public class Temperature {

    /**
     * Tag 可选
     */
    @Column(tag = true)
    String location;

    /**
     * Field 必选
     */
    @Column(name = "value")
    Long value;

    @Column(timestamp = true)
    Instant time;
}
