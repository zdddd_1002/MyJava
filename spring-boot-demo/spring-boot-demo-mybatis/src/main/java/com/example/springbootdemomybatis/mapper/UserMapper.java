package com.example.springbootdemomybatis.mapper;

import com.example.springbootdemomybatis.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @ProjectName: MyGitHub-OctoberZDi-MyJavaDemo
 * @ClassName: UserMapper
 * @Description: userMapper
 * @Author: zhangdi
 * @Date: 2020年12月26日 11:12
 **/
@Repository
@Mapper
public interface UserMapper {
    /*@Select("select * from user where id=#{id}")*/
    User selectById(@Param("id") Long id);

    // 看mybatis-config.xml的mappers节点配置，要么使用注解（类中的@select)要么使用mapper.xml中的，不能同时使用
    /*@Select("select * from user")*/
    List<User> selectAll();

    /**
     * mybatis-config.xml supportMethodsArguments=true 否则不生效
     *
     * @param name
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<User> selectByNamePage(@Param("name") String name, @Param("pageNum") int pageNum,
                                @Param("pageSize") int pageSize);

    List<User> selectByName(String name);

    List<User> selectByPageNumSize(User user);

    int insertUser(User user);

    int insertUsers(List<User> users1);

    int updateUserById(@Param("user") User user);

    int deleteUserById(@Param("id") String id);
}
