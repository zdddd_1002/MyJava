package com.example.springbootdemomybatis.test;

import com.example.springbootdemomybatis.entity.User;
import com.example.springbootdemomybatis.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * @ProjectName: MyJava
 * @ClassName: TestSelect
 * @Description: TestSelect
 * @Author: zhangdi
 * @Date: 2021年10月28日 11:18
 **/
public class TestSelect {
    public static void main(String[] args) throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);
        try (SqlSession sqlSession = build.openSession(true)) {
            System.out.println("查询全部");
            // 查询所有的数据
            List<User> selectAll = sqlSession.selectList("selectAll");
            selectAll.stream().forEach(System.out::println);

            // 根据id查询user
            System.out.println("根据id查询");
            User user = (User) sqlSession.selectOne("selectById", "130");
            System.out.println(user);

            System.out.println("限定行数查询！");
            List<User> selectAll1 = sqlSession.selectList("selectAll", null, new RowBounds(0, 5));
            System.out.println(selectAll1);

            System.out.println("模糊查询1");
            List<User> users = sqlSession.selectList("selectByName", "张迪1");
            System.out.println(users);

            System.out.println("模糊查询2");
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            List<User> users1 = mapper.selectByName("张迪");
            System.out.println(users1);

            System.out.println("模糊查询3 map");
            Map<Object, Object> objectObjectMap = sqlSession.selectMap("selectByName", "张迪2", "id");
            System.out.println(objectObjectMap);
            Object o = objectObjectMap.get("128");
            System.out.println("========");
            System.out.println(o);
        }
    }
}
