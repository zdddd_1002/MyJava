package com.example.springbootdemomybatis.test;

import com.example.springbootdemomybatis.entity.SysRole;
import com.example.springbootdemomybatis.mapper.SysRoleMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @ProjectName: MyJava
 * @ClassName: TestCache
 * @Description: MyBatis一级缓存
 * @Author: zhangdi
 * @Date: 2021年11月05日 09:09
 **/
public class TestCache1 {
    public static void main(String[] args) throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);
        // 一级缓存 ，存在于SQLSession的生命周期中，在【同一个SQLSession】中查询时。mybatis会把执行的方法和参数通过算法生成缓存的键值。
        // 将键值和查询结果存入一个Map对象中。如果同一个SqlSession中执行的方法和参数完全一直，那么通过算法会生成相同的键值，
        // 当map缓存对象中已经存在该键值时，则会返回缓存中的对象。
        // 缓存：键值key:执行的方法和参数通过算法生成键值，和查询结果 放入map
        try (SqlSession sqlSession = build.openSession()) {
            SysRoleMapper mapper = sqlSession.getMapper(SysRoleMapper.class);
            // 根据id查询
            SysRole sysRole1 = mapper.selectSysRoleById("1");
            System.out.println("第一次查询: " + sysRole1.getRoleName());
            // 重新赋值
            System.out.println("重新赋值");
            sysRole1.setRoleName("管理员AAA_2");
            SysRole sysRole2 = mapper.selectSysRoleById("1");
            System.out.println("第二次查询: " + sysRole2.getRoleName());
            // 获取sysRole1后重新设置了roleName的值，之后没有进行任何更新数据库操作。
            // 在获取sysRole2对象后，发现值一样，这就是因为mybatis的一级缓存。
            // sysRole1 ,sysRole2是同一个实例
            System.out.println(sysRole1 == sysRole2);

            // 如果要避免使用如上代码中sysRole2出现错误。我们可能以为获取的sysRole2是数据库中的数据，却不知道sysRole1的一个中心赋值会影响到sysRole2
            // 如果不想用优级缓存，可以使用flushCache="true"参数，关闭缓存
        }
    }
}
