package com.example.springbootdemomybatis.test;

import com.example.springbootdemomybatis.entity.SysRole;
import com.example.springbootdemomybatis.enums.Enabled;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @ProjectName: MyJava
 * @ClassName: TestSysRole
 * @Description: TestSysRole
 * @Author: zhangdi
 * @Date: 2021年11月02日 14:56
 **/
public class TestSysRole {
    public static void main(String[] args) throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);
        try (SqlSession sqlSession = build.openSession(true)) {
            System.out.println("单条查询-->根据id查询");
            SysRole role1 = sqlSession.selectOne("selectSysRoleById", "1");
            System.out.println("单条查询-->根据id查询:" + role1);

            System.out.println("多条查询->查询所有数据");
            List<SysRole> sysRoles = sqlSession.selectList("selectSysRoleAll");
            System.out.println("多条查询->查询所有数据: " + sysRoles);

            System.out.println("多条查询->条件获取需要的数据");
            Map<String, Object> params = new HashMap<String, Object>() {{
                this.put("roleName", "门卫");
                this.put("creator", "张小迪");
            }};
            List<SysRole> sysRoles2 = sqlSession.selectList("selectByCondition", params);
            System.out.println("多条查询->条件获取需要的数据:" + sysRoles2);

            System.out.println("choose when的使用查询");
            Map<String, Object> params2 = new HashMap<String, Object>() {{
                this.put("id", "1");
                this.put("roleName", "AA");
            }};
            List<SysRole> sysRoles3 = sqlSession.selectList("selectByIdOrRoleName", params2);
            System.out.println("choose when的使用查询:" + sysRoles3);

            System.out.println("单条插入->返回行数");
            SysRole sysRole = new SysRole() {{
                this.setRoleName("图书管理员" + new Random().nextInt(1000));
                this.setCreator("张迪" + new Random().nextInt(1000));
                this.setEnabled(Enabled.enabled);
            }};
            int insertSysRole = sqlSession.insert("insertSysRole", sysRole);
            System.out.println("单条插入->返回行数:" + insertSysRole);

            System.out.println("插入一条数据，返回自增的id");
            int returnId = sqlSession.insert("insertSysRoleReturnId", sysRole);
            System.out.println("插入一条数据，返回自增的id:" + sysRole.getId());

            System.out.println("批量插入多条");
            List<SysRole> sysRoles1 = new ArrayList<>();
            for (int i = 0; i < 3; i++) {
                int finalI = i;
                sysRoles1.add(new SysRole() {{
                    this.setRoleName("门卫" + finalI);
                    this.setCreator("张小迪" + finalI);
                    this.setEnabled(Enabled.enabled);
                }});
            }
            System.out.println(sysRoles instanceof Collection);
            int insertSysRoles = sqlSession.insert("insertSysRoles", sysRoles1);
            System.out.println("批量插入多条:" + insertSysRoles);

            System.out.println("数组批量插入多条");
            sysRoles1.forEach(item -> item.setRoleName("tom" + new Random().nextInt(100)));
            Object[] objects = sysRoles1.toArray();
            System.out.println("是数组吗？" + objects.getClass().isArray());
            int insertSysRoles2 = sqlSession.insert("insertSysRolesArray", objects);
            System.out.println("数组批量插入多条:" + insertSysRoles2);

            System.out.println("更新一条记录，根据id");
            role1.setRoleName("管理员AAA");
            role1.setCreator("张迪BBB");
            role1.setEnabled(Enabled.disabled);
            int updateSysRole = sqlSession.update("updateSysRole", role1);
            System.out.println("更新一条记录，根据id->条数: " + updateSysRole);

            System.out.println("参数为map是的动态update");
            Map<String, Object> updateParams = new HashMap<String, Object>() {{
                this.put("id", "2");
                this.put("role_name", "监督员AAA");
                this.put("creator", "张迪BBB");
                this.put("enabled", Enabled.unset);
            }};
            int updateSysRoleByMap = sqlSession.update("updateSysRoleByMap", updateParams);
            System.out.println("参数为map是的动态update:" + updateSysRoleByMap);


            System.out.println("删除一条记录根据id");
            int deleteSysRoleById = sqlSession.delete("deleteSysRoleById", 302);
            System.out.println("删除一条记录根据id:" + deleteSysRoleById);
            System.out.println("删除多条记录根据ids");
            String[] ids = new String[]{
                    "300", "301", "303"
            };
            int deleteSysRoleByIds = sqlSession.delete("deleteSysRolesByIds", Arrays.asList(ids));
            System.out.println("删除多条记录根据ids 条数:" + deleteSysRoleByIds);
        }
    }
}
