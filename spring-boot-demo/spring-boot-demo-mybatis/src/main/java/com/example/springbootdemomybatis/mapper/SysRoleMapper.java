package com.example.springbootdemomybatis.mapper;

import com.example.springbootdemomybatis.entity.SysRole;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.cache.decorators.FifoCache;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @ProjectName: MyJava
 * @ClassName: SysRoleMapper
 * @Description: SysRoleMapperF
 * @Author: zhangdi
 * @Date: 2021年11月02日 14:55
 **/
@Repository
// 如果相对注解方法启用二级缓存，需要进行CacheNamespace配置。
@CacheNamespace(eviction = FifoCache.class, flushInterval = 60000, size = 512, readWrite = true)
public interface SysRoleMapper {
    /**
     * selectById
     *
     * @param id
     * @return
     */
    SysRole selectSysRoleById(@Param("id") String id);

    /**
     * 获取所有 不能用selectAll有重名
     * 注意：此处为什么返回值类型是List<SysRole>而不是SysRole
     *
     * @return
     */
    List<SysRole> selectSysRoleAll();

    List<SysRole> selectByCondition(Map<String, Object> params);

    List<SysRole> selectByIdOrRoleName(Map<String, String> params);

    /**
     * 返回行数
     *
     * @param sysRole
     * @return
     */
    int insertSysRole(SysRole sysRole);

    /**
     * 返回自增的id
     *
     * @param sysRole
     * @return
     */
    int insertSysRoleReturnId(SysRole sysRole);

    int insertSysRoles(List<SysRole> sysRoles);

    int insertSysRolesArray(SysRole[] sysRoles);

    int updateSysRole(SysRole sysRole);

    /**
     * foreach用map更新表字段
     *
     * @param params
     * @return
     */
    int updateSysRoleByMap(Map<String, Object> params);

    int deleteSysRoleById(String id);

    int deleteSysRolesByIds(List<String> ids);
}
