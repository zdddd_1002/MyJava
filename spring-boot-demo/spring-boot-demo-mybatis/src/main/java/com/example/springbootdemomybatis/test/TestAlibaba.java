package com.example.springbootdemomybatis.test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ProjectName: MyJava
 * @ClassName: TestAlibaba
 * @Description: TestAlibaba
 * @Author: zhangdi
 * @Date: 2021年11月05日 14:31
 **/
public class TestAlibaba {
    public static void main(String[] args) {
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        System.out.println(LocalDate.now().lengthOfYear());
        System.out.println(LocalDate.now().lengthOfMonth());
        System.out.println(Calendar.JANUARY);
        System.out.println(Calendar.FEBRUARY);

        List<String> list = new ArrayList<>(2);
        list.add("aa");
        list.add("bb");
        System.out.println(list.stream().collect(Collectors.joining(",")));
        String[] strings = list.toArray(new String[0]);
        Arrays.stream(strings).forEach(System.out::println);

        Runtime.getRuntime().gc();
        char c = 'A';
        int num = 10;
        switch (c) {
            case 'B':
                num++;
            case 'A':
                num++;
            case 'Y':
                num++;
                break;
            default:
                num--;
        }
        System.out.println(num);
    }
}
