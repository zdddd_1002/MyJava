package com.example.springbootdemomybatis.controller;

import com.example.springbootdemomybatis.entity.User;
import com.example.springbootdemomybatis.mapper.UserMapper;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ProjectName: MyJava
 * @ClassName: UserController
 * @Description: controller.UserController
 * @Author: zhangdi
 * @Date: 2021年11月05日 10:52
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    UserMapper userMapper;

    @RequestMapping(value = "/listAll", method = RequestMethod.GET)
    public List<User> listAllUsers() {
        return userMapper.selectAll();
    }

    @RequestMapping(value = "/getUser/{id}", method = RequestMethod.GET)
    public User getUserById(@PathVariable Long id) {
        return userMapper.selectById(id);
    }
}
