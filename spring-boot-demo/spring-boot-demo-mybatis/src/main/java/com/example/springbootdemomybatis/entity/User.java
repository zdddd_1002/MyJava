package com.example.springbootdemomybatis.entity;


import java.sql.Timestamp;

/**
 * @ProjectName: MyGitHub-OctoberZDi-MyJavaDemo
 * @ClassName: User
 * @Description: user实体
 * @Author: zhangdi
 * @Date: 2020年12月26日 11:11
 **/
public class User {

    // 由于Java基本类型会有默认值，例如private int age；
    // 创建这个类是,age会有默认值0.因此在某些情况下,便无法实现age为null,并在在SQL中如果使用age!=null进行判断,结果总会为true.
    // 所以在实体类中不要使用基本类型。包括：byte,int,short,long,float,double,char,boolean

    private String id;
    private String name;
    private Integer age;
    private String email;

    /**
     * 用于分页参数
     */
    private Integer pageNum;
    private Integer pageSize;

    private Timestamp create_time;
    private Timestamp update_time;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public Timestamp getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Timestamp update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "=== User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                ", createTime=" + create_time +
                ", updateTime=" + update_time +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
