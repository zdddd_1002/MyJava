package com.example.springbootdemomybatis.test;

import com.example.springbootdemomybatis.entity.User;
import com.example.springbootdemomybatis.mapper.UserMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;
import java.util.List;

/**
 * @ProjectName: MyJava
 * @ClassName: TestPage1
 * @Description: TestPage1
 * @Author: zhangdi
 * @Date: 2021年11月01日 10:34
 **/
public class TestPage1 {

    public static void main(String[] args) throws Exception {
        // 通过Resources工具类将mybatis-config.xml读取到resourceAsStream
        InputStream resourceAsStream = Resources.getResourceAsStream("mybatis-config.xml");
        // SqlSessionFactoryBuilder建造类使用resourceAsStream创建SqlSessionFactory工厂对象。
        // 首先解析mybatis-config.xml配置文件，读取配置文件中的mappers配置后会读取全部的Mapper.xml进行具体方法解析
        // 解析完成后，SQLSessionFactory包含了所有的配置属性和执行SQL的信息。
        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);
        // SqlSession 不要忘记关闭：否则可能会导师数据库连接数过多，造成系统崩溃。
        try (SqlSession sqlSession = build.openSession(true)) {
            UserMapper mapper = sqlSession.getMapper(UserMapper.class);
            // 第一种：RowBounds方式的调用
            System.out.println("分页方式1：限定行数查询！");
            // *****RowBounds参数进行分页，侵入性最小，分页插件检测到使用RowBounds参数时，就会对查询进行物理分页。
            List<User> selectAll = sqlSession.selectList("selectAll", null, new RowBounds(0, 10));
            System.out.println("条数： " + selectAll.size() + "-->" + selectAll.get(0).getId() + "~" + selectAll.get(selectAll.size() - 1).getId());

            System.out.println("分页方式2：Mapper接口方式的调用，推荐使用这种方式");
            // *****
            PageHelper.startPage(1, 10);
            List<User> users = mapper.selectAll();
            System.out.println("条数： " + users.size() + "-->" + users.get(0).getId() + "~" + users.get(users.size() - 1).getId());

            System.out.println("分页方式3：Mapper接口方式的调用，推荐使用这种方式");
            PageHelper.offsetPage(1, 10);
            List<User> users2 = mapper.selectAll();
            // 如果想使用Page分页信息，需要强制转换为Page<E>
            Page pageUser = (Page) users2;
            System.out.println("Page信息->");
            System.out.println("  " + pageUser.getPageNum());
            System.out.println("  " + pageUser.getPageSize());
            System.out.println("  " + pageUser.getCountColumn());
            System.out.println("  " + pageUser.getPages());
            System.out.println("  " + pageUser.getDialectClass());
            System.out.println("  " + pageUser.getOrderBy());
            System.out.println("  " + pageUser.getEndRow());
            System.out.println("  " + pageUser.getPageSizeZero());
            System.out.println("  " + pageUser.getReasonable());
            System.out.println("  " + pageUser.getStartRow());
            System.out.println("  " + pageUser.getTotal());

            System.out.println("条数： " + users2.size() + "-->" + users2.get(0).getId() + "~" + users2.get(users.size() - 1).getId());

            System.out.println("分页方式4：参数方法调用，在Mapper接口中设置pageNum，pageSize接口，不需要在xml中处理后两个参数");
            List<User> users3 = mapper.selectByNamePage(null, 1, 10);
            System.out.println("条数： " + users3.size() + "-->" + users3.get(0).getId() + "~" + users3.get(users3.size() - 1).getId());

            // PageInfo 用法
            PageInfo pageInfo = new PageInfo(users3);
            System.out.println("PageInfo信息->");
            System.out.println("  getPageNum " + pageInfo.getPageNum());
            System.out.println("  getPageSize " + pageInfo.getPageSize());
            System.out.println("  getPages " + pageInfo.getPages());
            System.out.println("  getEndRow " + pageInfo.getEndRow());
            System.out.println("  getStartRow " + pageInfo.getStartRow());
            System.out.println("  getTotal " + pageInfo.getTotal());
            System.out.println("  getNavigateFirstPage " + pageInfo.getNavigateFirstPage());
            System.out.println("  getNavigateLastPage " + pageInfo.getNavigateLastPage());
            System.out.println("  getNavigatepageNums " + pageInfo.getNavigatepageNums());
            System.out.println("  getNavigatePages " + pageInfo.getNavigatePages());
            System.out.println("  getNextPage " + pageInfo.getNextPage());


            System.out.println("分页方式5：如果 pageNum 和 pageSize 存在于 User 对象中，只要参数有值，也会被分页");
            User user = new User() {
                {
                    this.setName("张迪");
                    this.setPageNum(1);
                    this.setPageSize(10);
                }
            };
            List<User> users4 = mapper.selectByPageNumSize(user);
            System.out.println("条数： " + users4.size() + "-->" + users4.get(0).getId() + "~" + users4.get(users4.size() - 1).getId());

            System.out.println("分页方式6：jdk8 lambda用法");
            PageInfo<Object> objectPageInfo = PageHelper.startPage(1, 10).doSelectPageInfo(() -> mapper.selectByPageNumSize(user));
            System.out.println(objectPageInfo);
        }
    }
}
