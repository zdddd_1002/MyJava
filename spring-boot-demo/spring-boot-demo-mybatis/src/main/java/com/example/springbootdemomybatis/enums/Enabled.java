package com.example.springbootdemomybatis.enums;

/**
 * @ProjectName: MyJava
 * @ClassName: Enabled
 * @Description: Enabled
 * @Author: zhangdi
 * @Date: 2021年11月04日 16:27
 **/
public enum Enabled {
    /**
     * 禁止，启用
     */
    disabled, enabled, unset;
}
