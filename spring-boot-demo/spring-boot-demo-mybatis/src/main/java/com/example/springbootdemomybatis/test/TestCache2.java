package com.example.springbootdemomybatis.test;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @ProjectName: MyJava
 * @ClassName: TestCache
 * @Description: MyBatis二级缓存
 * @Author: zhangdi
 * @Date: 2021年11月05日 09:09
 **/
public class TestCache2 {
    /*二级缓存全局配置mybatis-config.xml*/
    /*<!--二级缓存：默认开启 true ,全局性地开启或关闭所有映射器配置文件中已配置的任何缓存。-->
      <setting name="cacheEnabled" value="true"/>*/

    /*保证二级缓存的全局配置开启的情况下，给SysRoleMapper.xml开启二级缓存只需要添加<cache/>元素即可*/

    public static void main(String[] args) throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);

        try (SqlSession sqlSession = build.openSession()) {

        }
    }
}
