package com.example.springbootdemoflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDemoFlowApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoFlowApplication.class, args);
    }

}
