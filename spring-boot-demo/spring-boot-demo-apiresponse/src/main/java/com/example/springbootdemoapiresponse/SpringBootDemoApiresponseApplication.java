package com.example.springbootdemoapiresponse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDemoApiresponseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoApiresponseApplication.class, args);
    }

}
