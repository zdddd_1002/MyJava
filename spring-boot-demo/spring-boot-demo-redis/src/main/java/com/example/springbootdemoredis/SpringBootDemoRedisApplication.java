package com.example.springbootdemoredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhangdi03
 */
@SpringBootApplication
public class SpringBootDemoRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoRedisApplication.class, args);
    }

}
