package com.example.springbootdemoinfluxdb.demo.query;

import com.example.springbootdemoinfluxdb.demo.model.InfluxInfo;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.QueryApi;

/**
 * @ProjectName: MyJava
 * @ClassName: ExampleInfluxRawQuery
 * @Description: ExampleInfluxRawQuery Raw查询允许直接处理原始CSV响应
 * @Author: zhangdi
 * @Date: 2021年09月28日 16:44
 **/
public class ExampleInfluxRawQuery {
    public static void main(String[] args) {
        String fluxQl = "from(bucket:\"mydb\") |> range(start: -1h) |> filter(fn: (r) => r._measurement == " +
                "\"temperature\")";
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(InfluxInfo.url, InfluxInfo.token.toCharArray(),
                InfluxInfo.org, InfluxInfo.bucket);

        QueryApi queryApi = influxDBClient.getQueryApi();
        String s = queryApi.queryRaw(fluxQl);
        System.out.println(s);

        influxDBClient.close();
    }
}
