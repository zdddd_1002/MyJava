package com.example.springbootdemoinfluxdb.demo.query;

import com.example.springbootdemoinfluxdb.demo.model.InfluxInfo;
import com.example.springbootdemoinfluxdb.demo.model.Temperature;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.QueryApi;

import java.util.List;

/**
 * @ProjectName: MyJava
 * @ClassName: ExampleInfluxQuerySyncPojo
 * @Description: 同步查询并不适用于大型查询结果，因为可以解除Flux响应的绑定。
 * @Author: zhangdi
 * @Date: 2021年09月28日 10:58
 **/
public class ExampleInfluxQuerySyncPojo {
    public static void main(String[] args) {
        String fluxQl = "from(bucket:\"mydb\") |> range(start:-2h) |> filter(fn:(r)=>r._measurement == \"temperature\")";
        InfluxDBClient influxDbClient = InfluxDBClientFactory.create(InfluxInfo.url, InfluxInfo.token.toCharArray(),
                InfluxInfo.org, InfluxInfo.bucket);
        // Query data
        QueryApi queryApi = influxDbClient.getQueryApi();

        // 同步查询提供了将FluxRecords映射到POJO的可能性
        List<Temperature> temperatures = queryApi.query(fluxQl, Temperature.class);
        for (Temperature temperature : temperatures) {
            System.out.println(temperature.location + ": " + temperature.longitude + " at " + temperature.time);
        }

        influxDbClient.close();
    }
}
