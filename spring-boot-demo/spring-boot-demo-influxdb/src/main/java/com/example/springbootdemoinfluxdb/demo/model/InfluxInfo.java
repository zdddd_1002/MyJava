package com.example.springbootdemoinfluxdb.demo.model;

/**
 * @ProjectName: MyJava
 * @ClassName: InfluxInfo
 * @Description: influxdb info
 * @Author: zhangdi
 * @Date: 2021年09月28日 10:32
 **/
public class InfluxInfo {

    private InfluxInfo() {
    }

    public static final String url = "http://10.201.52.33:8086";
    public static final String token = "7mkqr7pSahQjiIS4GUYEqPVX92I4u6od24IDa5NxeW3oPy9ir99yvlXtxsZ6VfoFXtr51-hG4by8oinsX50adg==";
    public static final String org = "inspur";
    public static final String bucket = "mydb";
}
