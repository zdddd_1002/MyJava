package com.example.springbootdemoinfluxdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhangdi
 */
@SpringBootApplication
public class SpringBootDemoInfluxdbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoInfluxdbApplication.class, args);
    }

}
