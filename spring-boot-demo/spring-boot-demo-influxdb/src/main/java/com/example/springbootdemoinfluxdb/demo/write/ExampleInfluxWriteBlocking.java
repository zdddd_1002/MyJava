package com.example.springbootdemoinfluxdb.demo.write;

import com.example.springbootdemoinfluxdb.demo.model.InfluxInfo;
import com.example.springbootdemoinfluxdb.demo.model.Temperature;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.WriteApiBlocking;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;

import java.time.Instant;

/**
 * @ProjectName: MyJava
 * @ClassName: ExampleInfluxDbWrite
 * @Description: ExampleInfluxDbWrite
 * @Author: zhangdi
 * @Date: 2021年09月27日 15:36
 **/
public class ExampleInfluxWriteBlocking {

    public static void main(String[] args) {
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(InfluxInfo.url, InfluxInfo.token.toCharArray(),
                InfluxInfo.org, InfluxInfo.bucket);

        // write data
        WriteApiBlocking writeApiBlocking = influxDBClient.getWriteApiBlocking();

        // 1、write by data point
        System.out.println("write by data point...");
        Point point = Point.measurement("temperature").addTag("location", "north").addTag("longitude", "777.888").addField(
                "value", 55d).time
                (Instant.now().toEpochMilli(), WritePrecision.MS);

        writeApiBlocking.writePoint(point);

        // 2、write by line protocol
        System.out.println("write by line protocol...");
      /*  writeApiBlocking.writeRecord(WritePrecision.MS,
                "temperature,location=east,longitude=222.555 value=55.3333 " + Instant.now().toEpochMilli());
*/
        writeApiBlocking.writeRecord(WritePrecision.MS,
                "cpu,Ident=10.200.52.33  fieldKey=55.3333 " + Instant.now().toEpochMilli());
        // 3、write by pojo
        System.out.println("write by pojo...");
        Temperature temperature = new Temperature();
        // tag
        temperature.location = "south";
        temperature.longitude = 44444.222;

        temperature.latitude = "1111.222";
        temperature.time = Instant.now();

        writeApiBlocking.writeMeasurement(WritePrecision.NS, temperature);

        influxDBClient.close();
    }
}
