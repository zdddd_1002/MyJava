package com.example.springbootdemoinfluxdb.demo.query;

import com.example.springbootdemoinfluxdb.demo.model.InfluxInfo;
import com.example.springbootdemoinfluxdb.demo.model.Temperature;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.QueryApi;

import java.util.List;

/**
 * @ProjectName: MyJava
 * @ClassName: SynchronousQueryPojo
 * @Description: SynchronousQueryPojo
 * @Author: zhangdi
 * @Date: 2021年09月28日 14:05
 **/
public class SynchronousQueryPojo {
    private static char[] token = "my-token".toCharArray();
    private static String org = "my-org";

    public static void main(final String[] args) {
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(InfluxInfo.url, InfluxInfo.token.toCharArray(),
                InfluxInfo.org, InfluxInfo.bucket);

        // Query data
        String flux = "from(bucket:\"mydb\") |> range(start: -1h) |> filter(fn: (r) => r._measurement == " +
                "\"temperature\")";

        QueryApi queryApi = influxDBClient.getQueryApi();

        // Map to POJO
        List<Temperature> temperatures = queryApi.query(flux, Temperature.class);
        for (Temperature temperature : temperatures) {
            System.out.println(temperature.location + temperature.latitude + ": " + temperature.longitude + " at " + temperature.time);
        }

        influxDBClient.close();
    }
}
