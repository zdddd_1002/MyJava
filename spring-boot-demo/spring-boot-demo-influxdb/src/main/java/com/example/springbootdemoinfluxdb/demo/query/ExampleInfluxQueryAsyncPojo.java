package com.example.springbootdemoinfluxdb.demo.query;

import com.example.springbootdemoinfluxdb.demo.model.InfluxInfo;
import com.example.springbootdemoinfluxdb.demo.model.Temperature;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.QueryApi;

/**
 * @ProjectName: MyJava
 * @ClassName: ExampleInfluxQueryAsyncPojo
 * @Description: ExampleInfluxQueryAsyncPojo
 * 异步查询提供了处理未绑定查询的可能性，并允许用户处理异常，停止接收更多结果并通知所有数据已到达。
 * @Author: zhangdi
 * @Date: 2021年09月28日 11:20
 **/
public class ExampleInfluxQueryAsyncPojo {
    public static void main(String[] args) throws InterruptedException {
        String fluxQl = "from(bucket:\"mydb\") |> range(start: -1h) |> filter(fn: (r) => r._measurement == " +
                "\"temperature\")";
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(InfluxInfo.url, InfluxInfo.token.toCharArray(),
                InfluxInfo.org, InfluxInfo.bucket);
        QueryApi queryApi = influxDBClient.getQueryApi();
        queryApi.query(fluxQl, Temperature.class, (cancellable, temperature) ->
                System.out.println(temperature.location + ": " + temperature.longitude + " at " + temperature.time));
        Thread.sleep(5000);

        influxDBClient.close();
    }
}
