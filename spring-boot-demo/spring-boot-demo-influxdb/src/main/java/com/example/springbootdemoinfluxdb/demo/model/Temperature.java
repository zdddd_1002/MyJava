package com.example.springbootdemoinfluxdb.demo.model;

import com.influxdb.annotations.Column;
import com.influxdb.annotations.Measurement;

import java.time.Instant;

/**
 * @ProjectName: MyJava
 * @ClassName: Temperature
 * @Description:Temperature
 * @Author: zhangdi
 * @Date: 2021年09月28日 14:40
 **/
@Measurement(name = "temperature")
public class Temperature {
    @Column(tag = true)
    public String location;

    @Column(tag = true)
    public String latitude;

    /**
     * 经度
     */
    @Column(name = "value")
    public Double longitude;

    @Column(timestamp = true)
    public Instant time;
}
