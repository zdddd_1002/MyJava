package com.example.springbootdemoinfluxdb.demo.bucket;

import com.example.springbootdemoinfluxdb.demo.model.InfluxInfo;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.domain.*;

import java.util.Arrays;

/**
 * @ProjectName: MyJava
 * @ClassName: ExampleInfluxBucket
 * @Description: ExampleInfluxBucket
 * @Author: zhangdi
 * @Date: 2021年09月29日 09:27
 **/
public class ExampleInfluxBucket {
    public static void main(String[] args) {
        // Query data
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(InfluxInfo.url, InfluxInfo.token.toCharArray(),
                InfluxInfo.org, InfluxInfo.bucket);

        //创建一个名为“iot_bucket”的桶，数据保留时间为3600秒
        BucketRetentionRules bucketRetentionRules = new BucketRetentionRules();
        bucketRetentionRules.everySeconds(3600);

        // 创建bucket  influxDBClient.getBucketApi()

        // 系统中已经存在的orgID：4fbf159407fa1319
        Bucket bucket = influxDBClient.getBucketsApi().createBucket("myBucket", bucketRetentionRules,
                "4fbf159407fa1319");

        // 创建令牌
        PermissionResource permissionResource = new PermissionResource();
        permissionResource.setId(bucket.getId());
        permissionResource.setOrgID("4fbf159407fa1319");
        permissionResource.setType(PermissionResource.TypeEnum.BUCKETS);

        // 读权限
        Permission readPerm = new Permission();
        readPerm.setResource(permissionResource);
        readPerm.setAction(Permission.ActionEnum.READ);

        // 写权限
        Permission writePerm = new Permission();
        writePerm.setResource(permissionResource);
        writePerm.setAction(Permission.ActionEnum.WRITE);

        Authorization authorization = influxDBClient.getAuthorizationsApi()
                .createAuthorization("4fbf159407fa1319", Arrays.asList(readPerm, writePerm));

        System.out.println(authorization);
        System.out.println("Token:" + authorization.getToken());
        influxDBClient.close();
    }
}
