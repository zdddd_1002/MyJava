package com.example.springbootdemoinfluxdb.demo;

import com.example.springbootdemoinfluxdb.demo.model.InfluxInfo;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;

/**
 * @ProjectName: MyJava
 * @ClassName: ExampleInfluxDbBucket
 * @Description: ExampleInfluxDbBucket
 * @Author: zhangdi
 * @Date: 2021年09月28日 10:36
 **/
public class ExampleInfluxDbBucket {
    public static void main(String[] args) {
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(InfluxInfo.url, InfluxInfo.token.toCharArray(),
                InfluxInfo.org, InfluxInfo.bucket);

    }
}
