package com.example.springbootdemoinfluxdb.demo.bucket;

import com.example.springbootdemoinfluxdb.demo.model.InfluxInfo;
import com.influxdb.client.BucketsApi;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.domain.Bucket;
import com.influxdb.client.domain.Label;
import com.influxdb.client.domain.ResourceMember;
import com.influxdb.client.domain.ResourceOwner;

import java.util.List;

/**
 * @ProjectName: MyJava
 * @ClassName: ExampleInfluxBucketQuery
 * @Description: ExampleInfluxBucketQuery bucket查询
 * @Author: zhangdi
 * @Date: 2021年09月29日 09:55
 **/
public class ExampleInfluxBucketQuery {

    public static void main(String[] args) {

        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(InfluxInfo.url, InfluxInfo.token.toCharArray(),
                InfluxInfo.org, InfluxInfo.bucket);
        BucketsApi bucketsApi = influxDBClient.getBucketsApi();

        // mydb：7990a6386b5dfb54
        System.out.println("members-->");
        List<ResourceMember> members = bucketsApi.getMembers("7990a6386b5dfb54");
        members.forEach(item -> System.out.println(item));

        System.out.println("labels-->");
        List<Label> labels = bucketsApi.getLabels("7990a6386b5dfb54");
        labels.forEach(item -> System.out.println(item));

        System.out.println("owners-->");
        List<ResourceOwner> owners = bucketsApi.getOwners("7990a6386b5dfb54");
        owners.forEach(item -> System.out.println(item));

        System.out.println("buckets-->");
        List<Bucket> buckets = influxDBClient.getBucketsApi().findBuckets();
        buckets.forEach(item -> {
            System.out.println(item);
        });

    }
}
