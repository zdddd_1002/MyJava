package com.example.springbootdemoinfluxdb.demo.query;

import com.example.springbootdemoinfluxdb.demo.model.InfluxInfo;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.QueryApi;

/**
 * @ProjectName: MyJava
 * @ClassName: ExampleInfluxRawQueryAsync
 * @Description: ExampleInfluxRawQueryAsync 异步版本允许逐行处理:
 * @Author: zhangdi
 * @Date: 2021年09月28日 16:48
 **/
public class ExampleInfluxRawQueryAsync {
    public static void main(String[] args) throws InterruptedException {
        String fluxQl = "from(bucket:\"mydb\") |> range(start: -1h) |> filter(fn: (r) => r._measurement == " +
                "\"temperature\")";
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(InfluxInfo.url, InfluxInfo.token.toCharArray(),
                InfluxInfo.org, InfluxInfo.bucket);
        QueryApi queryApi = influxDBClient.getQueryApi();

        queryApi.queryRaw(fluxQl, (cancellable, line) -> {
            System.out.println("Response: " + line);
        });

        Thread.sleep(5_000);

        influxDBClient.close();
    }
}
