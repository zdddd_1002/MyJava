package com.example.springbootdemoinfluxdb.demo.query;

import com.example.springbootdemoinfluxdb.demo.model.InfluxInfo;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.QueryApi;

/**
 * @ProjectName: MyJava
 * @ClassName: ExampleInfluxQueryASync
 * @Description: ExampleInfluxQueryASync
 * 异步查询提供了处理未绑定查询的可能性，并允许用户处理异常，停止接收更多结果并通知所有数据已到达。
 * @Author: zhangdi
 * @Date: 2021年09月28日 11:20
 **/
public class ExampleInfluxQueryAsync {
    public static void main(String[] args) throws InterruptedException {
        String fluxQl = "from(bucket:\"mydb\") |> range(start:-2h)";
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(InfluxInfo.url, InfluxInfo.token.toCharArray(),
                InfluxInfo.org, InfluxInfo.bucket);
        QueryApi queryApi = influxDBClient.getQueryApi();
        queryApi.query(fluxQl, (cancellable, fluxRecord) -> {
            // 使用FluxRecord的回调
            // 对象具有取消方法来停止异步查询
            System.out.println(fluxRecord.getValueByKey("_field") + " | " + fluxRecord.getValueByKey("_value") + " | " + fluxRecord.getMeasurement() + " | " + fluxRecord.getTime());
        }, throwable -> {
            // 使用任何错误通知的回调
            System.out.println("Error occurred: " + throwable.getMessage());
        }, () -> {
            // 使用关于流成功结束的通知的回调
            System.out.println("Query completed");
        });

        Thread.sleep(5000);

        influxDBClient.close();
    }
}
