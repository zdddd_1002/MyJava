package com.example.springbootdemoinfluxdb.demo.influx18.controller;

import org.influxdb.InfluxDB;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ProjectName: MyJava
 * @ClassName: TestInflux18Controller
 * @Description: d
 * @Author: zhangdi
 * @Date: 2021年11月09日 11:15
 **/
@RestController
@RequestMapping(value = "/influx18")
public class TestInflux18Controller {
    @Resource
    InfluxDB influxDB;

    @RequestMapping(value = "/query1")
    public void getData() {
        String sql = "select * from uptime";
        influxDB.setDatabase("collectdb");
        QueryResult query = influxDB.query(new Query(sql));
        if (!query.hasError() && !query.getResults().isEmpty()) {
            query.getResults().forEach(System.out::println);
        }
    }
}
