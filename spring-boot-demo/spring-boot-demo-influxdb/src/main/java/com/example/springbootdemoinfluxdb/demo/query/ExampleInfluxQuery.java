package com.example.springbootdemoinfluxdb.demo.query;

import com.example.springbootdemoinfluxdb.demo.model.InfluxInfo;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.QueryApi;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @ProjectName: MyJava
 * @ClassName: ExampleInfluxQuery
 * @Description: ExampleInfluxQuery
 * @Author: zhangdi
 * @Date: 2021年09月27日 17:13
 **/
public class ExampleInfluxQuery {

    public static void main(String[] args) {
        // Query data
        String fluxQl = "from(bucket:\"mydb\") |> range(start:-2h)";
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(InfluxInfo.url, InfluxInfo.token.toCharArray(),
                InfluxInfo.org, InfluxInfo.bucket);
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> query = queryApi.query(fluxQl);
        final int[] index = {0};
        query.forEach(
                item -> {
                    if (index[0] == 0) {
                        System.out.println("label--->" + item.getColumns().stream().map(i -> i.getLabel()).collect(Collectors.joining(
                                ",")));
                        System.out.println("=================");
                        System.out.println("measurement   | table |     field   | temperature |     value   " +
                                "  |     start   |     stop   |     time ");
                        index[0]++;
                    }
                    List<FluxRecord> records = item.getRecords();
                    records.forEach(
                            fluxRecord -> {
                                System.out.println(fluxRecord.getMeasurement() + "   |" + fluxRecord.getTable() + "|   " + fluxRecord.getField() + "  |   "
                                        + fluxRecord.getValueByKey("_value") + "  |   " + fluxRecord.getValue() +
                                        "  |   " + fluxRecord.getStart() + "  |   " + fluxRecord.getStop() + "  |   " + fluxRecord.getTime());
                            }
                    );
                }
        );

        influxDBClient.close();
    }
}
