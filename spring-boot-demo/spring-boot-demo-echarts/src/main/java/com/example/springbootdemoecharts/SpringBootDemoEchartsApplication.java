package com.example.springbootdemoecharts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDemoEchartsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoEchartsApplication.class, args);
    }

}
