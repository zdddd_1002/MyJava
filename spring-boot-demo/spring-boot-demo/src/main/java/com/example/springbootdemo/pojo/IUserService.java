package com.example.springbootdemo.pojo;

/**
 * @ProjectName: MyGitHub-OctoberZDi-MyJavaDemo
 * @ClassName: IUserService
 * @Description: dd
 * @Author: zhangdi
 * @Date: 2020年02月23日 12:04
 **/
public interface IUserService {
    void getComment();
}
