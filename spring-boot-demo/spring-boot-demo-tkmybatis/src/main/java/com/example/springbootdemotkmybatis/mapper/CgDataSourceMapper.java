package com.example.springbootdemotkmybatis.mapper;

import com.example.springbootdemotkmybatis.po.CgDataSource;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface CgDataSourceMapper extends Mapper<CgDataSource> {
}
