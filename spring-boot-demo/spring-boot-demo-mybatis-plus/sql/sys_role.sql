-- mydb.sys_role definition

CREATE TABLE `sys_role` (
                            `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
                            `role_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '角色名',
                            `enabled` int(11) DEFAULT NULL COMMENT '有效标志',
                            `creator` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
                            `create_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '创建时间',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=646 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
