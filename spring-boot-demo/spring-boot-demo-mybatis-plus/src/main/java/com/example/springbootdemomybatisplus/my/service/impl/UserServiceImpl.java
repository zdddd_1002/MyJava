package com.example.springbootdemomybatisplus.my.service.impl;

import com.example.springbootdemomybatisplus.my.entity.User;
import com.example.springbootdemomybatisplus.my.mapper.UserMapper;
import com.example.springbootdemomybatisplus.my.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangdi
 * @since 2021-11-08
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
