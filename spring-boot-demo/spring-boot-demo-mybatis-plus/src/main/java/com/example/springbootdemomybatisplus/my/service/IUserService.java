package com.example.springbootdemomybatisplus.my.service;

import com.example.springbootdemomybatisplus.my.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangdi
 * @since 2021-11-08
 */
public interface IUserService extends IService<User> {

}
