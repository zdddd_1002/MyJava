package com.example.springbootdemomybatisplus.my.mapper;

import com.example.springbootdemomybatisplus.my.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangdi
 * @since 2021-11-08
 */
public interface UserMapper extends BaseMapper<User> {

}
