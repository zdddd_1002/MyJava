package com.example.springbootdemomybatisplus.my.enums;

import com.baomidou.mybatisplus.annotation.IEnum;

/**
 * @ProjectName: MyJava
 * @ClassName: Enabled
 * @Description: Enabled
 * @Author: zhangdi
 * @Date: 2021年11月04日 16:27
 **/
public enum Enabled implements IEnum<Integer> {
    // 枚举属性，实现 IEnum 接口如下：
    /**
     * 禁止，启用
     */
    disabled(0), enabled(1), unset(2);

    private int value;

    Enabled(int i) {
        this.value = i;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }
}
