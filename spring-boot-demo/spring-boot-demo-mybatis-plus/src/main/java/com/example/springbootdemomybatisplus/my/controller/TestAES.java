package com.example.springbootdemomybatisplus.my.controller;

import com.baomidou.mybatisplus.core.toolkit.AES;

/**
 * @ProjectName: MyJava
 * @ClassName: TestAES
 * @Description: TestAES  https://baomidou.com/guide/safety.html
 * @Author: zhangdi
 * @Date: 2021年11月09日 16:00
 **/
public class TestAES {
    public static void main(String[] args) {
        String randomKey = AES.generateRandomKey();
        System.out.println("randomKey--->" + randomKey);

        String userName = "root";
        String encrypt = AES.encrypt(userName, randomKey);
        System.out.println("加密--->" + encrypt);
        String decrypt = AES.decrypt(encrypt, randomKey);
        System.out.println("解密--->" + decrypt);
    }
}
