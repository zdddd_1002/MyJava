package com.example.springbootdemomybatisplus.my.service.impl;

import com.example.springbootdemomybatisplus.my.entity.SysRole;
import com.example.springbootdemomybatisplus.my.mapper.SysRoleMapper;
import com.example.springbootdemomybatisplus.my.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zhangdi
 * @since 2021-11-08
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Resource
    SysRoleMapper sysRoleMapper;

    @Override
    public List<SysRole> getAll() {
        return sysRoleMapper.selectList(null);
    }
}
