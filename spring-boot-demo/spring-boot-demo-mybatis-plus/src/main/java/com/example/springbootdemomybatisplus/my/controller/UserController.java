package com.example.springbootdemomybatisplus.my.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhangdi
 * @since 2021-11-08
 */
@Controller
@RequestMapping("/my/user")
public class UserController {

}
