package com.example.springbootdemomybatisplus.my.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.springbootdemomybatisplus.my.entity.SysRole;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zhangdi
 * @since 2021-11-08
 */
public interface ISysRoleService extends IService<SysRole> {
    /**
     * @return
     */
    List<SysRole> getAll();
}
