package com.example.springbootdemomybatisplus.my.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.springbootdemomybatisplus.my.entity.SysRole;
import com.example.springbootdemomybatisplus.my.enums.Enabled;
import com.example.springbootdemomybatisplus.my.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * <p>
 * 前端控制器
 * </p>
 * service CURD 接口
 *
 * @author zhangdi
 * @since 2021-11-08
 */
@RestController
@RequestMapping("/sysRole")
public class SysRoleWithServiceController {
    ISysRoleService sysRoleService;

    @Autowired
    public void setSysRoleService(ISysRoleService service) {
        this.sysRoleService = service;
    }

    @RequestMapping(value = "/getCount")
    public int getCount() {
        return sysRoleService.count();
    }

    @RequestMapping(value = "/getQueryCount")
    public int getQueryCount() {
        LambdaQueryWrapper<SysRole> lambda = new QueryWrapper<SysRole>().lambda();
        lambda.like(SysRole::getRoleName, "AA");
        return sysRoleService.count(lambda);
    }

    @RequestMapping(value = "/getAll")
    public List<SysRole> getAll() {
        List<SysRole> all = null;
        try {
            all = sysRoleService.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return all;
    }

    @RequestMapping(value = "/getById/{id}")
    public SysRole getById(@PathVariable String id) {
        SysRole sysRole = sysRoleService.getById(id);
        if (sysRole != null) {
            return sysRole;
        }
        return null;
    }

    @RequestMapping(value = "/getOne/{id}")
    public SysRole getOne(@PathVariable String id) {
        QueryWrapper<SysRole> sysRoleQueryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<SysRole> eq = sysRoleQueryWrapper.lambda().eq(SysRole::getId, id);
        SysRole sysRole = sysRoleService.getOne(eq);
        if (sysRole != null) {
            return sysRole;
        }
        return null;
    }

    @RequestMapping(value = "/getMap")
    public Map<String, Object> getMap() {
        QueryWrapper<SysRole> sysRoleQueryWrapper = new QueryWrapper<>();
        sysRoleQueryWrapper.lambda().like(SysRole::getRoleName, "董事长");
        // 请注意：getMap 查询一条记录
        return sysRoleService.getMap(sysRoleQueryWrapper);
    }

    @RequestMapping(value = "save")
    public boolean save() {
        SysRole sysRole = new SysRole() {{
            this.setCreator("张迪" + new Random().nextInt(10000));
            this.setEnabled(Enabled.enabled);
            this.setRoleName("门卫" + new Random().nextInt(10000));
        }};
        return sysRoleService.saveOrUpdate(sysRole);
    }

    @RequestMapping(value = "saveBatch")
    public boolean saveBatch() {
        List<SysRole> sysRoles = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            sysRoles.add(
                    new SysRole() {{
                        this.setCreator("张筱满" + new Random().nextInt(10000));
                        this.setEnabled(Enabled.enabled);
                        this.setRoleName("董事长" + new Random().nextInt(100));
                    }});
        }
        //return sysRoleService.saveBatch(sysRoles);
        return sysRoleService.saveOrUpdateBatch(sysRoles);
    }

    @RequestMapping(value = "/removeById/{id}")
    public boolean removeById(@PathVariable String id) {
        return sysRoleService.removeById(id);
    }

    @RequestMapping(value = "/remove")
    public boolean remove() {
        QueryWrapper<SysRole> sysRoleQueryWrapper = new QueryWrapper<>();
        sysRoleQueryWrapper.lambda().like(SysRole::getCreator, "张筱满");
        return sysRoleService.remove(sysRoleQueryWrapper);
    }

    @RequestMapping(value = "/updateById/{id}")
    public boolean updateById(@PathVariable String id) {
        System.out.println(id);
        SysRole byId = sysRoleService.getById(id);
        System.out.println(byId);
        byId.setRoleName("AAA" + new Random().nextInt(1000));
        byId.setCreator("BBB" + new Random().nextInt(1000));
        // 乐观锁自己更新，不需要特殊设置，因为加了注解@version:自动+1
        //byId.setVersion(byId.getVersion() + 1);
        return sysRoleService.updateById(byId);
    }

    @RequestMapping(value = "/selectPage/{current}/{size}/{roleName}")
    public IPage<SysRole> selectPage(@PathVariable int current, @PathVariable int size, @PathVariable String roleName) {
        Page<SysRole> sysRolePage = new Page<>(current, size);
        QueryWrapper<SysRole> sysRoleQueryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<SysRole> like = sysRoleQueryWrapper.lambda().like(SysRole::getRoleName, roleName);
        return sysRoleService.page(sysRolePage, like);
    }
}
