package com.example.springbootdemomybatisplus.autocg;

/* 参考URL： https://baomidou.com/guide/generator-new.html#%E5%AE%89%E8%A3%85*/
/* 适用于版本3.5.1 */

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * @ProjectName: MyJava
 * @ClassName: FastCodeGEnerator
 * @Description: FastCodeGEnerator
 * @Author: zhangdi
 * @Date: 2021年11月08日 11:18
 **/
public class FastCodeGEnerator {
    private static String url = "jdbc:mysql://10.201.52.33:3306/mydb?setUnicode=true&characterEncoding=utf8&useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8&allowPublicKeyRetrieval=true";
    private static String user = "root";
    private static String password = "1234";

    public static void main(String[] args) {
        String modelName = "/spring-boot-demo/spring-boot-demo-mybatis-plus";
        String projectPath = System.getProperty("user.dir") + modelName;
        String outDir = projectPath + "/src/main/java";
        FastAutoGenerator.create(url, user, password).globalConfig(builder -> {
                    builder.author("zhangdi")   // 设置作者
                            //.enableSwagger()    // 开启swagger模式
                            .fileOverride()     // 开启文件覆盖
                            .outputDir(outDir) // 指定文件输出目录
                            .disableOpenDir()  // 禁止打开输出目录
                            .commentDate("yyyy-MM-dd"); // 注释日期
                }).packageConfig(builder -> {
                    builder.parent("com.example.springbootdemomybatisplus")   // 父报名：默认值com.baomidou
                            .moduleName("my")       // 父包模块名：默认值 无
                            .service("service")     // service名，默认service
                            .serviceImpl("service.impl") // serviceImpl，默认service.impl
                            .mapper("mapper")          // mapper 名，默认：mapper
                            // .xml("xml") // 默认值：mapper.xml
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, projectPath + "/src/main" +
                                    "/resources/com/example/springbootdemomybatisplus/my/mapper"));
                }).strategyConfig(builder -> {
                    builder.addInclude("sys_role", "user")
                            .addTablePrefix("t_");
                })
                .templateEngine(new FreemarkerTemplateEngine()).execute();
    }
}
