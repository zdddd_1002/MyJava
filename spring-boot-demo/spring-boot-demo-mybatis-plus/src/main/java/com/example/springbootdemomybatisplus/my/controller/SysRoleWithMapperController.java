package com.example.springbootdemomybatisplus.my.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.springbootdemomybatisplus.my.entity.SysRole;
import com.example.springbootdemomybatisplus.my.enums.Enabled;
import com.example.springbootdemomybatisplus.my.mapper.SysRoleMapper;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @ProjectName: MyJava
 * @ClassName: SysRoleWithMapperController
 * @Description: SysRoleWithMapperController
 * @Author: zhangdi
 * mapper CURD 接口
 * @Date: 2021年11月09日 10:05
 **/
@RestController
@RequestMapping("/sysRole2")
public class SysRoleWithMapperController {
    @Resource
    SysRoleMapper sysRoleMapper;

    @RequestMapping(value = "insert")
    public int insert() {
        SysRole sysRole = new SysRole() {{
            this.setCreator("张迪" + new Random().nextInt(10000));
            this.setEnabled(Enabled.enabled);
            this.setRoleName("门卫" + new Random().nextInt(100));
        }};

        return sysRoleMapper.insert(sysRole);
    }

    @RequestMapping(value = "deleteById/{id}")
    public int deleteById(@PathVariable String id) {
        return sysRoleMapper.deleteById(id);
    }

    @RequestMapping(value = "deleteByWrapper")
    public int deleteByWrapper() {
        LambdaQueryWrapper<SysRole> lambdaQueryWrapper = new QueryWrapper<SysRole>().lambda().like(SysRole::getCreator, "BBB");
        return sysRoleMapper.delete(lambdaQueryWrapper);
    }

    /**
     * 此处演示了乐观锁@version的使用
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "updateById/{id}")
    public int updateById(@PathVariable Long id) {
        SysRole sysRole = new SysRole();
        sysRole.setId(id);
        sysRole.setRoleName("AAA" + new Random().nextInt(1000));
        sysRole.setCreator("BBB" + new Random().nextInt(1000));
        return sysRoleMapper.updateById(sysRole);
    }

    @RequestMapping(value = "selectById/{id}")
    public SysRole selectById(@PathVariable String id) {
        return sysRoleMapper.selectById(id);
    }

    @RequestMapping(value = "selectBatchIds/{ids}")
    public List<SysRole> selectBatchIds(@PathVariable String ids) {
        String[] split = ids.split("-");
        return sysRoleMapper.selectBatchIds(Arrays.asList(split));
    }

    @RequestMapping(value = "/selectPage/{current}/{size}/{roleName}")
    public IPage<SysRole> selectPage(@PathVariable int current, @PathVariable int size, @PathVariable String roleName) {
        Page<SysRole> sysRolePage = new Page<>(current, size);
        QueryWrapper<SysRole> sysRoleQueryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<SysRole> like = sysRoleQueryWrapper.lambda().like(SysRole::getRoleName, roleName);
        return sysRoleMapper.selectPage(sysRolePage, like);
    }

    public void test() {
        QueryWrapper<SysRole> sysRoleQueryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<SysRole> lambda = sysRoleQueryWrapper.lambda();

        UpdateWrapper<SysRole> updateWrapper = new UpdateWrapper<>();
        LambdaUpdateWrapper<SysRole> lambda1 = updateWrapper.lambda();
    }
}
