package com.example.springbootdemomybatisplus.my.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.example.springbootdemomybatisplus.my.enums.Enabled;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangdi
 * @since 2021-11-08
 */
@TableName("sys_role")
public class SysRole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色ID IdType.AUTO 数据库id自增
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 角色名
     */
    // 排除非表中字段 三种方式：
    //        1、private transient String noColumn;
    //        2、private static String noColumn;
    //        3、@TableField(exist = false)
    private String roleName;

    /**
     * 有效标志
     */
    private Enabled enabled;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     * <p>
     * 注意！这里需要标记为填充字段
     * 1)标记为填充字段，搭配MyMetaObjectHandler使用
     * 2)数据库字段设置默认值：mysql---> current_timestamp()
     * <p>
     * FieldFill
     */
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Version
    @TableField(update = "%s+1", updateStrategy = FieldStrategy.IGNORED)
    private Integer version;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Enabled getEnabled() {
        return enabled;
    }

    public void setEnabled(Enabled enabled) {
        this.enabled = enabled;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "SysRole{" +
                "id=" + id +
                ", roleName=" + roleName +
                ", enabled=" + enabled +
                ", creator=" + creator +
                ", createTime=" + createTime +
                "}";
    }
}
