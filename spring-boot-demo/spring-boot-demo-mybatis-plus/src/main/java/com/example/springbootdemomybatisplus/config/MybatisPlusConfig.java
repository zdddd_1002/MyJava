package com.example.springbootdemomybatisplus.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ProjectName: MyJava
 * @ClassName: MybatisPlusConfig
 * @Description: MybatisPlusConfig
 * 参考URL：https://baomidou.com/guide/interceptor.html#%E4%BD%BF%E7%94%A8%E6%96%B9%E5%BC%8F-%E4%BB%A5%E5%88%86%E9%A1%B5%E6%8F%92%E4%BB%B6%E4%B8%BE%E4%BE%8B
 * @Author: zhangdi
 * @Date: 2021年11月09日 14:43
 **/
@Configuration
@MapperScan(value = "com.example.springbootdemomybatisplus.**.mapper.**")  //配置Mapper.java的扫描其中之方法二
public class MybatisPlusConfig {


    /**
     * 配置Mapper.java的扫描其中之方法二
     *
     * @return
     */
   /* @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setBasePackage("com.example.springbootdemomybatisplus.**.mapper.**");
        return mapperScannerConfigurer;
    }*/

    /**
     * 新的分页插件,一缓和二缓遵循mybatis的规则,需要设置 MybatisConfiguration#useDeprecatedExecutor = false 避免缓存出现问题(该属性会在旧插件移除后一同移除)
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        // 乐观锁 version
        // 乐观锁的实现方式：
        // 1、取出记录时获取当前的version。
        // 2、更新时，带上version。
        // 3、执行更新时，set version = newVersion where version = oldVersion
        // 4、如果version不对，就更新失败
        mybatisPlusInterceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        return mybatisPlusInterceptor;
    }

    // 如何序列化枚举值为数据库存储值？
    // https://baomidou.com/guide/enum.html#jackson
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer customizer() {
        return builder -> builder.featuresToEnable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
    }
}
