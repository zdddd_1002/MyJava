package com.example.springbootdemoschedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling // 开启定时任务生效配置
@EnableAsync   // 开启异步生效配置
public class SpringBootDemoScheduleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDemoScheduleApplication.class, args);
    }

}
