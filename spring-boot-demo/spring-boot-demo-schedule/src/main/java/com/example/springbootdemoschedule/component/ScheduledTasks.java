package com.example.springbootdemoschedule.component;

import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @ProjectName: MyJava
 * @ClassName: ScheduledTasks
 * @Description: component.ScheduledTasks
 * @Author: zhangdi
 * @Date: 2021年10月29日 10:44
 **/
//@Component
public class ScheduledTasks {
    //
    //@Scheduled(fixedRate = 5000) ：上一次开始执行时间点之后5秒再执行
    //@Scheduled(fixedDelay = 5000) ：上一次执行完毕时间点之后5秒再执行
    //@Scheduled(initialDelay = 1000, fixedRate = 5000) ：第一次延迟1秒后执行，之后按fixedRate的规则每5秒执行一次
    //@Scheduled(cron = "*/5*****") ：通过cron表达式定义规则

    /**
     * fixedRate = 5000 每过5秒执行一次任务
     */
    @Scheduled(fixedRate = 5000)

    public void reportCurrentTime() {
        System.out.println("现在时间：" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:MM:ss")));
    }
}
