package com.example.springbootdemoschedule;

import com.example.springbootdemoschedule.component.AsyncTasks;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.Future;

@SpringBootTest
class SpringBootDemoScheduleApplicationTests {

    @Autowired
    AsyncTasks asyncTasks;

    @Test
    @SneakyThrows
    public void executeTask() throws Exception {
        long start = System.currentTimeMillis();

        Future<String> task1 = asyncTasks.doTaskOne();
        Future<String> task2 = asyncTasks.doTaskTwo();

        while (true) {
            if (task1.isDone() && task2.isDone()) {
                // 三个任务都调用完成，退出循环等待
                break;
            }
            Thread.sleep(1000);
        }

        long end = System.currentTimeMillis();

        System.out.println("任务全部完成，总耗时：" + (end - start) + "毫秒");

    }
}
