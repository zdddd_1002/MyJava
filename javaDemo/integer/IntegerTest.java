package javaDemo.integer;

/**
 * @ProjectName: MyGitHub-OctoberZDi-MyJavaDemo
 * @ClassName: IntegerTest
 * @Description:
 * @Author: zhangdi
 * @Date: 2021年01月29日 14:57
 **/
public class IntegerTest {
    public static void main(String[] args) {
        Integer a = 127;
        Integer b = 127;
        System.out.println("a==b:" + (a == b));

        Integer c = 128;
        Integer d = 128;
        System.out.println("c==d:" + (c == d));
    }
}
