package javaDemo.data;

/**
 * @ProjectName: MyGitHub-OctoberZDi-MyJavaDemo
 * @ClassName: IPeople
 * @Description: 人类接口
 * @Author: zhangdi
 * @Date: 2020年11月23日 11:12
 **/
public interface IPeople {
    /**
     * say
     */
    void say();
}
