package javaDemo.fanxing;

/**
 * @ProjectName: MyJava
 * @ClassName: Demo2
 * @Description:
 * @Author: zhangdi
 * @Date: 2021年11月22日 11:00
 **/
public class Demo2<T> {

    private T type;
    private String name;

    public T getType() {
        return type;
    }

    public void setType(T type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Demo2{" +
                "type=" + type +
                ", name='" + name + '\'' +
                '}';
    }

    public T getType2() {
        return this.getType();
    }

    public Demo2<T> getObj() {
        return this;
    }
}
