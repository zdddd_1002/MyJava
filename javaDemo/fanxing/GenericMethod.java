package javaDemo.fanxing;

/**
 * @ProjectName: MyJava
 * @ClassName: GenericMethod
 * @Description:
 * @Author: zhangdi
 * @Date: 2022年02月09日 10:13
 **/
public class GenericMethod {

    /**
     * 说明：1）public 与返回值中间的<T>非常重要，可以理解为此方法声明为泛型。
     * 2）只有声明了<T>的方法才是泛型方法，泛型类中的使用了泛型的成员并不是泛型方法。
     * 3）<T>表明该方法将使用泛型类型T，此时才可以在方法中使用泛型类型T.
     * 4)与泛型类的定义一样，此处T可以随意任何标识，常见的如 T，E，K等形式的参数常用于标识泛型。
     *
     * @param tClass
     * @param <T>
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public <T> T genericMethod(Class<T> tClass) throws InstantiationException, IllegalAccessException {
        return tClass.newInstance();
    }

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        GenericMethod genericMethod = new GenericMethod();
        System.out.println(genericMethod.genericMethod(Class.forName("javaDemo.fanxing.Stu")));
    }
}

class Stu<T> {
    String name;
    Integer age;

    /**
     * 这个方法显然是有问题的，在编译器会给我们提示这样的错误信息"cannot reslove symbol E"
     * 因为在类的声明中并未声明泛型E，所以在使用E做形参和返回值类型时，编译器会无法识别。
     */
    /*public E setKey(E key) {
        this.key = key;
    }*/

    T type;

    public T getType() {
        return type;
    }

    public void setType(T type) {
        this.type = type;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
