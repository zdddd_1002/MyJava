package javaDemo.fanxing;

/**
 * @ProjectName: MyJava
 * @ClassName: MyGeneric
 * @Description: 当实现泛型接口的类，未传入泛型实参时
 * @Author: zhangdi
 * @Date: 2022年02月09日 09:57
 * <p>
 * 未传入泛型实参时，与泛型类的定义相同，在声明类的时候，需将泛型的声明也一起加到类中
 * 即：class FruitGenerator<T> implements Generator<T>{
 * 如果不声明泛型，如：class FruitGenerator implements Generator<T>，编译器会报错："Unknown class"
 */
public class MyGeneric<T> implements IGeneric<T> {
    /**
     * ddd
     *
     * @return dd
     */
    @Override
    public T next() {
        return null;
    }
}

