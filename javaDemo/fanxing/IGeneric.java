package javaDemo.fanxing;

/**
 * @ProjectName: MyJava
 * @ClassName: MyInterface
 * @Description: 泛型接口
 * https://www.cnblogs.com/coprince/p/8603492.html
 * @Author: zhangdi
 * @Date: 2022年02月09日 09:53
 **/
public interface IGeneric<T> {
    /**
     * ddd
     *
     * @return dd
     */
    T next();
}
