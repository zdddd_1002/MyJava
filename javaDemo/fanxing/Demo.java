package javaDemo.fanxing;

import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: MyJava
 * @ClassName: Demo
 * @Description: Demo
 * @Author: zhangdi
 * @Date: 2021年11月22日 10:44
 **/
public class Demo<T> {

    T type;

    String name;

    public T getType() {
        return type;
    }

    public void setType(T type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Demo@@@@@{" +
                "type=" + type +
                ", name='" + name + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Demo<String> stringDemo = new Demo<>();
        stringDemo.setType("People");
        stringDemo.setName("China");
        System.out.println(stringDemo);

        Demo<Integer> integerDemo = new Demo<>();
        integerDemo.setType(100);
        integerDemo.setName("Zhangdi");
        System.out.println(integerDemo);

        List<String> aa = new ArrayList<>();
        aa.add("AAA");
        aa.add("BBB");
        aa.add("CCC");
        System.out.println(getList(aa));
        System.out.println(getListFirst(aa));

        List<Integer> aa2 = new ArrayList<>();
        aa2.add(1);
        aa2.add(2);
        aa2.add(3);
        System.out.println(getList(aa2));
        System.out.println(getListFirst(aa2));

        //     Demo2

        Demo2<String> stringDemo2 = new Demo2<>();
        stringDemo2.setType("z");
        stringDemo2.setName("zhangdi");
        System.out.println(stringDemo2);

        System.out.println(stringDemo2.getType2());
        System.out.println(stringDemo2.getObj());
    }

    /**
     * 这个只能传递T类型的数据
     * 返回值 就是Demo<T> 实例化传递的对象类型
     *
     * @param data
     * @return
     */
    private static <T> T getListFirst(List<T> data) {
        if (data == null || data.size() == 0) {
            return null;
        }
        return data.get(0);
    }

    private static <T> List<T> getList(List<T> demoList) {
        return demoList;
    }
}
