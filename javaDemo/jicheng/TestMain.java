package javaDemo.jicheng;

/**
 * @ProjectName: MyGitHub-OctoberZDi-MyJavaDemo
 * @ClassName: TestMain
 * @Description:
 * @Author: zhangdi
 * @Date: 2020年08月19日 13:49
 **/
public class TestMain {
    public static void main(String[] args) {
        TestA testA = new TestA();
        testA.a();
        testA.b();
    }
}
