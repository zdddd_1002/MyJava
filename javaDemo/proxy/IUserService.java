package javaDemo.proxy;

/**
 * @ProjectName: MyGitHub-OctoberZDi-MyJavaDemo
 * @ClassName: IUserService
 * @Description: user接口
 * @Author: zhangdi
 * @Date: 2020年05月26日 13:57
 **/
public interface IUserService {
    /**
     *
     */
    void say();
}
