package javaDemo.proxy.dongtai;

/**
 * @ProjectName: MyGitHub-OctoberZDi-MyJavaDemo
 * @ClassName: IUserService
 * @Description: 接口类
 * @Author: zhangdi
 * @Date: 2020年05月26日 14:06
 **/
interface IUserDao {
    void say(String sth);
}
